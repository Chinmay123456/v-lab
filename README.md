ROUND1

Experiment: ACID-BASE TITRATION USING pH METER

Discipline: Engineering

Lab: Engineering Chemistry

Contents:

1. Focus Area
2. Learning Objectives
3. Instructional Strategy
4. Task and Assessment Questions


    1. Focus Area: Experimentation and Data Analysis

The students will get to know the concept of acid-base titration, using pH meter, and the relation between normality and volume for finding the normality of the base. Finally analysing the results by comparing with the standard values provided.

2. Learning Objectives and Cognitive Level

| Sr No. | Learning Objective | Cognitive Level | Action Verb |
| --- | --- | --- | --- |
| 1. | User will be able to: Identify the apparatus used in the experiment: pipette, burette,beaker,pH meter.| Recall | Identify |
| 2. | User will be able to: Define normality,pH and the working of pH meter. | Recall | Define |
| 3. | User will be able to: Calculate the normality of the base using the normality-volume relationship . | Apply | Calculate |
| 4. | User will be able to: Graphically identify the equivalence point using the readings to pH meter. | Analyze | Examine |
| 5. | User will be able to: Examine the change in pH when base is added to the acid gradually. | Analyze | Examine |


3. Instructional Strategy

Name of Instructional Strategy: Expository

Assessment Method: Formative Assessment

Description: Instructional Strategy will be implemented in the stimulator as follows:

Students will identify the various components as per arrangement of the setup. With this they will get a clear idea about the concept and purpose of each component. They will calibrate the pH meter. The students will then be able to set the value for concentration and volume of analyte and titrant. Based on these reading students will be enabled to calculate the pH of the solution as titration proceeds. These values will be tabulated and a required graph will be plotted and the equivalence point will be determined. After performing the experiment, Student shall give the answer to the questions asked related to Acid base titration and pH meter.

4. Task and Assessment Question

   *  What is Constant Burette Reading?
   *  What is the difference between combination electrode and reference electrode?
   *  Write the procedure of two point standarization of pH meter.
   *  How should I store my pH electrode?
   *  Draw a neat and labelled diagram of the glass electrode.
  
